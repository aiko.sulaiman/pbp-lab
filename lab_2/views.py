from django.shortcuts import render
from .models import Note
from django.http.response import HttpResponse
from django.core import serializers

# Create your views here.
def index(request):
    note = Note.objects.all()
    response = {'note': note}
    return render(request, 'lab_2.html', response)

def xml(data):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(data):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")

