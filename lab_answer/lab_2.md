1. Apakah perbedaan antara JSON dan XML?
JSON yang merupakan singkatan dari “JavaScript Object Notation” merupakan format pertukaran data berbasis teks yang menggunakan tipe data teks dan angka untuk merepresentasikan data terstruktur dalam format yang dapat dibaca manusia. XML yang merupakan singkatan dari “Extensive Markup Language” yang merupakan bahasa markup untuk mengkode data terstruktur dan dapat dibaca oleh mesin atau manusia. 
Perbedaan:
    - XML didasari dari bahasa markup SGML (Standard Generalized Markup Language) sementara JSON dari Javascript
    - XML tidak mendukung penggunaan array secara langsung sementara JSON iya
    - Comments bisa ditulis pada JSON, tetapi tidak bisa pada XML
    - Data XML disimpan sebagai tree structure sementara data di JSON disimpan seperti map dengan pasangan key dan value

2. Apakah perbedaan antara HTML dan XML?
    - HTML fokus pada mengatur format tampilan halaman website dan menampilkan data sementara XML fokus pada menyimpan dan transfer data
    - HTML tidak case sensitive, XML case sensitive
    - HTML mempunyai tags yang sudah terdefinisi, sementara tags di XML lebih fleksibel dan bisa didefinisikan sesuai kebutuhan